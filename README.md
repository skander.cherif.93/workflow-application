# Getting Started

### Setup Application

To set database create postgres database named _**ncq\_workflow**_ and role _**workflow**_ then import sql script from %projectDirectory%/database \(you can use dbeaver or pgadmin client \)

Now your postgres db should be up and running

You can now start the workflow application \(port: 8083\), just execute this command in project directory

```
mvn spring-boot:run
```

### Test Application

1. #### _**OpenApi**_

Use openapi documentation to test apis

```
http://localhost:8083/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/workflow-category-resource2
```

2. #### _**Postman**_

First import collection \(NCQ Workflow.postman\_collection.json\) from %projectDirectory%/postman, then use NCQ Workflow collection to test apis

![](postman/import-collection.jpg)