package com.ncq.workflow;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncq.workflow.controller.WorkflowCategoryResource;
import com.ncq.workflow.controller.WorkflowResource;
import com.ncq.workflow.domain.Workflow;
import com.ncq.workflow.domain.WorkflowCategory;
import com.ncq.workflow.repository.WorkflowCategoryRepository;
import com.ncq.workflow.repository.WorkflowRepository;
import com.ncq.workflow.service.WorkflowCategoryService;
import com.ncq.workflow.service.WorkflowService;

@RunWith(SpringRunner.class)
@SpringBootTest /* (webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) */
@Transactional
public class WorkflowApplicationTests {

	@Autowired
	private WorkflowRepository workflowRepository;

	@Autowired
	private WorkflowCategoryRepository workflowCategoryRepository;

	@Autowired
	private WorkflowCategoryService workflowCategoryService;

	@Autowired
	private WorkflowService workflowService;

	private MockMvc restWorkflowMockMvc;
	private MockMvc restWorkflowCategoryMockMvc;

	private Workflow testWorkflow;
	private WorkflowCategory testWorkflowCategory;

	private static final Integer DEFAULT_STATUS = 1;

	private static final String DAFEAULT_WORKFLOW_NAME = "test-wkf";
	private static final String DAFEAULT_CATEGORY_WORKFLOW_NAME = "test-category-wkf";

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final WorkflowCategoryResource workflowCategoryMockResource = new WorkflowCategoryResource(
				workflowCategoryService);
		final WorkflowResource workflowMockResource = new WorkflowResource(workflowService);
		this.restWorkflowMockMvc = MockMvcBuilders.standaloneSetup(workflowMockResource)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
		this.restWorkflowCategoryMockMvc = MockMvcBuilders.standaloneSetup(workflowCategoryMockResource)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
	}

	@BeforeEach
	public void initTest() {
		testWorkflow = new Workflow().name(DAFEAULT_WORKFLOW_NAME).status(DEFAULT_STATUS);
		testWorkflowCategory = new WorkflowCategory().name(DAFEAULT_CATEGORY_WORKFLOW_NAME).status(DEFAULT_STATUS);
	}

	@Test
	public void getAllCategoriesTest() throws Exception {

		workflowCategoryRepository.saveAndFlush(testWorkflowCategory);
		workflowRepository.saveAndFlush(testWorkflow);

		// Get all the categories
		// Testing using assertJ
		MvcResult mvcResult = restWorkflowCategoryMockMvc.perform(
				MockMvcRequestBuilders.get("/api/v1/workflow-categories").accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode workflowCategoryResponse = mapper.readTree(mvcResult.getResponse().getContentAsString());
		assertThat(workflowCategoryResponse).isNotNull();
		assertThat(workflowCategoryResponse.get(0).get("idCategory").toString()).isNotNull()
				.isEqualTo(testWorkflowCategory.getIdCategory().toString());

		assertThat(workflowCategoryResponse.get(0).get("name").asText()).isNotNull()
				.isEqualTo(testWorkflowCategory.getName());

		/*
		 * assertThat(workflowCategoryResponse.get(0).get("description").toString())
		 * .isNotNull() .isEqualTo(testWorkflowCategory.getDescription());
		 * 
		 * assertThat(workflowCategoryResponse.get(0).get("logo").toString())
		 * .isNotNull() .isEqualTo(testWorkflowCategory.getLogo());
		 */

		assertThat(workflowCategoryResponse.get(0).get("status").toString()).isNotNull()
				.isEqualTo(testWorkflowCategory.getStatus().toString());
	}

	@SuppressWarnings("deprecation")
	@Test
	public void getAllWorkflowsTest() throws Exception {
		// Initialize the database
		testWorkflowCategory = workflowCategoryRepository.saveAndFlush(testWorkflowCategory);
		// assigning the categories to the current workflow
		List<WorkflowCategory> categories = new ArrayList<>();
		categories.add(testWorkflowCategory);
		testWorkflow.setWorkflowCategories(categories);
		testWorkflow = workflowRepository.save(testWorkflow);
		this.restWorkflowMockMvc.perform(get("/api/v1/workflows")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType((MediaType.APPLICATION_JSON)))
				.andExpect(jsonPath("$.[*].status").value(hasItem(testWorkflow.getStatus())))
				.andExpect(jsonPath("$.[*].idWorkflow").value(hasItem(testWorkflow.getIdWorkflow().intValue())))
				.andExpect(jsonPath("$.[*].name").value(hasItem(testWorkflow.getName())))
				.andExpect(jsonPath("$.[*].description").value(hasItem(testWorkflow.getDescription())))
				.andExpect(jsonPath("$.[*].status").value(hasItem(testWorkflow.getStatus())));
	}

	@Test
	public void getWorkflowsWithEmptyResultTest() throws Exception {

		MvcResult mvcResult = restWorkflowMockMvc.perform(
				MockMvcRequestBuilders.get("/api/v1/workflows?name=nothing").accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(200, status);
		assertEquals(MediaType.APPLICATION_JSON_VALUE, mvcResult.getResponse().getContentType());
		assertEquals("[]", content);

	}

	@Test
	public void getAllWorkflowUsingNameTest() throws Exception {
		// Initialize the database
		testWorkflowCategory = workflowCategoryRepository.saveAndFlush(testWorkflowCategory);
		// assigning the categories to the current workflow
		List<WorkflowCategory> categories = new ArrayList<>();
		categories.add(testWorkflowCategory);
		testWorkflow.setWorkflowCategories(categories);
		testWorkflow = workflowRepository.save(testWorkflow);
		this.restWorkflowMockMvc.perform(get("/api/v1/workflows?name=" + DAFEAULT_WORKFLOW_NAME)).andDo(print())
				.andExpect(status().isOk()).andExpect(content().contentType((MediaType.APPLICATION_JSON)))
				.andExpect(jsonPath("$.[*].status").value(hasItem(testWorkflow.getStatus())))
				.andExpect(jsonPath("$.[*].idWorkflow").value(hasItem(testWorkflow.getIdWorkflow().intValue())))
				.andExpect(jsonPath("$.[*].name").value(hasItem(testWorkflow.getName())))
				.andExpect(jsonPath("$.[*].description").value(hasItem(testWorkflow.getDescription())))
				.andExpect(jsonPath("$.[*].status").value(hasItem(testWorkflow.getStatus())));
	}

	@Test
	public void getWorkflowsWithStatusTest() throws Exception {
		// Initialize the database
		testWorkflowCategory = workflowCategoryRepository.save(testWorkflowCategory);
		// assigning the categories to the current workflow
		List<WorkflowCategory> categories = new ArrayList<>();
		categories.add(testWorkflowCategory);
		testWorkflow.setWorkflowCategories(categories);
		testWorkflow.setName(DAFEAULT_WORKFLOW_NAME);
		testWorkflow = workflowRepository.save(testWorkflow);
		// Get all the workflows with status
		restWorkflowMockMvc.perform(get("/api/v1/workflows?status=" + DEFAULT_STATUS)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.[*].idWorkflow").value(hasItem(testWorkflow.getIdWorkflow().intValue())))
				.andExpect(jsonPath("$.[*].name").value(hasItem(testWorkflow.getName())))
				.andExpect(jsonPath("$.[*].description").value(hasItem(testWorkflow.getDescription())))
				.andExpect(jsonPath("$.[*].status").value(hasItem(testWorkflow.getStatus())));
	}
}