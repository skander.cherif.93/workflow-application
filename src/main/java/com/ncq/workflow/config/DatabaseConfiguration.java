package com.ncq.workflow.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configure the JPA repository.
 */
@Configuration
@EnableJpaRepositories("com.ncq.workflow.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {
}
