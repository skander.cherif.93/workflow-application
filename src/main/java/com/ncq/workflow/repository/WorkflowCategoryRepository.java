package com.ncq.workflow.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ncq.workflow.domain.WorkflowCategory;

/**
 * Spring Data JPA repository for the {@link WorkflowCategory} entity.
 */
@Repository
public interface WorkflowCategoryRepository extends JpaRepository<WorkflowCategory, Long> {
}
