package com.ncq.workflow.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ncq.workflow.domain.Workflow;


/**
 * Spring Data JPA repository for the {@link Workflow} entity.
 */
@Repository
public interface WorkflowRepository extends JpaRepository<Workflow, Long> {
	/*@Query("select wkf from Workflow wkf where " + "(:name is null or wkf.name = :name) and " +
	 "(:idCategories is null or wkf.workflowCategories.idCategory in (:idCategories)) and " + 
	 "(:status is null or wkf.status = :status)")*/
	
	@Query("select wkf from Workflow wkf where " +
    "(:name is null or wkf.name = :name) and " +
    "(:idCategories is null  or ':idCategories' IN (select 'idCategory' from wkf.workflowCategories)) and " + 
	"(:status is null or wkf.status = :status)")
	Page<Workflow> findWorkflowCatWithNameOrStatus(String name, List<Long> idCategories,Integer status, Pageable pageable);
}
