package com.ncq.workflow.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * A workflow.
 */
@Entity
@Table(name = "ncq_workflow")
public class Workflow implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long idWorkflow;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Min(1)
    @Max(5)
    private Integer status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="ncq_workflow_variants",
            joinColumns=@JoinColumn(name="IdWorkflow"),
            inverseJoinColumns=@JoinColumn(name="IdVariant")
    )
    @JsonIgnore
    private List<Workflow> variants = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "variants")
    /*@JoinTable(name="ncq_workflow_variants",
            joinColumns=@JoinColumn(name="IdVariant"),
            inverseJoinColumns=@JoinColumn(name="IdWorkflow")
    )*/
    @Transient
    @JsonIgnore
    private List<Workflow> variantOfCat = new ArrayList<>();

   
    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    private List<WorkflowCategory> workflowCategories;

    
    
    
    public Long getIdWorkflow() {
        return idWorkflow;
    }

    public void setIdWorkflow(Long idWorkflow) {
        this.idWorkflow = idWorkflow;
    }

    public Workflow idWorkflow(Long idWorkflow){
        this.idWorkflow = idWorkflow;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Workflow name(String name){
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Workflow Description(String description){
        this.description = description;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Workflow status(Integer status){
        this.status = status;
        return this;
    }

    public List<Workflow> getVariants() {
        return variants;
    }

    public void setVariants(List<Workflow> variants) {
        this.variants = variants;
    }

    public Workflow variants(List<Workflow> variants) {
        this.variants = variants;
        return this;
    }

    public List<Workflow> getVariantOf() {
        return variantOfCat;
    }

    public void setVariantOf(List<Workflow> variantOf) {
        this.variantOfCat = variantOf;
    }

    public List<WorkflowCategory> getWorkflowCategories() {
        return workflowCategories;
    }

    public void setWorkflowCategories(List<WorkflowCategory> categories) {
        this.workflowCategories = categories;
    }

    public Workflow workflowCategories(List<WorkflowCategory> workflowCategories){
        this.workflowCategories = workflowCategories;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workflow)) {
            return false;
        }
        return idWorkflow != null && idWorkflow.equals(((Workflow) o).idWorkflow);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Workflow{" +
                "IdWorkflow=" + idWorkflow +
                ", Name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ", status=" + status +
                ", variants=" + variants +
                ", variantOfCategory=" + variantOfCat +
                ", workflowCategories=" + workflowCategories +
                '}';
    }

	
}
