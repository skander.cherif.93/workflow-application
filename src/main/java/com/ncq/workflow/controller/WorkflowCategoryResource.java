package com.ncq.workflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ncq.workflow.domain.WorkflowCategory;
import com.ncq.workflow.service.WorkflowCategoryService;

/**
 * REST controller for managing workflow.
 */
@RestController
@RequestMapping("/api/v1")
public class WorkflowCategoryResource {
    private final Logger log = LoggerFactory.getLogger(WorkflowCategoryResource.class);

    private final WorkflowCategoryService workflowCategoryService;

    public WorkflowCategoryResource(WorkflowCategoryService workflowCategoryService) {
        this.workflowCategoryService = workflowCategoryService;
    }

    /**
     * {@code GET  /workflow-categories} : get all workflow categories.
     *
     * @return the list of all categories.
     */
    @GetMapping("/workflow-categories")
    public List<WorkflowCategory> getAllCategories() {
        log.info("REST request to get all WorkflowCategories");      
        return workflowCategoryService.getAllCategoriesWorkflow();
    }
    
    
    

}
