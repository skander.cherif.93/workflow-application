package com.ncq.workflow.controller;

import com.ncq.workflow.domain.Workflow;
import com.ncq.workflow.domain.WorkflowCategory;
import com.ncq.workflow.service.WorkflowService;
import com.ncq.workflow.utils.PaginationUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

/**
 * REST controller for managing workflow.
 */
@RestController
@RequestMapping("/api/v1")
public class WorkflowResource {
    private final Logger log = LoggerFactory.getLogger(WorkflowResource.class);

    private final WorkflowService workflowService;

    public WorkflowResource(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    /**
     * {@code GET  /workflows} : get the list of workflows.
     *
     * @param name
     * @param categoryIds
     * @param status
     * @param pageable
     * @return the list of workflows.
     */
    @GetMapping("/workflows")
    public ResponseEntity<List<Workflow>> getAllWorkflows(@RequestParam(required = false) String name, @RequestParam(required = false) List<Long> categoryId, @RequestParam(required = false) @Min(1) @Max(5) Integer status, Pageable pageable) {
        log.info("REST request to get all Workflows");
    	final Page<Workflow> page = workflowService.getWorkflows(name, categoryId, status, pageable);
        HttpHeaders headers = PaginationUtils.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
