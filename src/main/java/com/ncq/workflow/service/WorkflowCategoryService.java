package com.ncq.workflow.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ncq.workflow.domain.WorkflowCategory;
import com.ncq.workflow.repository.WorkflowCategoryRepository;

/**
 * Service for managing workflow.
 */
@Service
@Transactional
public class WorkflowCategoryService {
    private final WorkflowCategoryRepository workflowCategoryRepository;

    public WorkflowCategoryService(WorkflowCategoryRepository workflowCategoryRepository) {
        this.workflowCategoryRepository = workflowCategoryRepository;
    }

    public List<WorkflowCategory> getAllCategoriesWorkflow(){
        return workflowCategoryRepository.findAll();
    }
}
