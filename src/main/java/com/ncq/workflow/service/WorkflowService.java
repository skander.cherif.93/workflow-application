package com.ncq.workflow.service;

import com.ncq.workflow.domain.Workflow;
import com.ncq.workflow.domain.WorkflowCategory;
import com.ncq.workflow.repository.WorkflowCategoryRepository;
import com.ncq.workflow.repository.WorkflowRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for managing workflow.
 */
@Service
@Transactional
public class WorkflowService {
    private final WorkflowRepository workflowRepository;

    public WorkflowService(WorkflowRepository workflowRepository) {
        this.workflowRepository = workflowRepository;
    }


    public Page<Workflow> getWorkflows(String name, List<Long> categoryIds, Integer status, Pageable pageable){
        return workflowRepository.findWorkflowCatWithNameOrStatus(name, categoryIds,status, pageable);
    }

}
