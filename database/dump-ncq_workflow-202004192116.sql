--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.7

-- Started on 2020-04-19 21:16:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2841 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16766)
-- Name: ncq_workflow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ncq_workflow (
    id_workflow bigint NOT NULL,
    description character varying(255),
    name character varying(255),
    status integer,
    CONSTRAINT ncq_workflow_status_check CHECK (((status <= 5) AND (status >= 1)))
);


ALTER TABLE public.ncq_workflow OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16775)
-- Name: ncq_workflow_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ncq_workflow_category (
    id_category bigint NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    description character varying(255),
    logo character varying(255),
    name character varying(255),
    status integer,
    parent_id bigint,
    CONSTRAINT ncq_workflow_category_status_check CHECK (((status <= 5) AND (status >= 1)))
);


ALTER TABLE public.ncq_workflow_category OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16784)
-- Name: ncq_workflow_variants; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ncq_workflow_variants (
    id_workflow bigint NOT NULL,
    id_variant bigint NOT NULL
);


ALTER TABLE public.ncq_workflow_variants OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16787)
-- Name: ncq_workflow_workflow_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ncq_workflow_workflow_categories (
    workflow_id_workflow bigint NOT NULL,
    workflow_categories_id_category bigint NOT NULL
);


ALTER TABLE public.ncq_workflow_workflow_categories OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16790)
-- Name: sequence_generator; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sequence_generator
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_generator OWNER TO postgres;

--
-- TOC entry 2831 (class 0 OID 16766)
-- Dependencies: 196
-- Data for Name: ncq_workflow; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ncq_workflow (id_workflow, description, name, status) FROM stdin;
1	this is workflow 1	workflow 1	1
2	this is workflow 2	workflow 2	1
3	this is workflow 3	workflow 3	2
4	this is workflow 4	workflow 4	3
5	this is workflow 5	workflow 5	5
\.


--
-- TOC entry 2832 (class 0 OID 16775)
-- Dependencies: 197
-- Data for Name: ncq_workflow_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ncq_workflow_category (id_category, created_at, updated_at, description, logo, name, status, parent_id) FROM stdin;
1	2020-09-03 11:54:02	2020-09-03 11:54:02	wkfcat1	none	workflow1	1	1
2	2020-09-03 11:54:02	2020-09-03 11:54:02	wkfcat2	none	workflow2	2	2
3	2020-09-03 11:54:02	2020-09-03 11:54:02	wkfcat3	none	workflow3	1	1
4	2020-09-03 11:54:02	2020-09-03 11:54:02	wkfcat4	none	workflow4	3	1
5	2020-09-03 11:54:02	2020-09-03 11:54:02	wkfcat5	none	workflow5	3	3
\.


--
-- TOC entry 2833 (class 0 OID 16784)
-- Dependencies: 198
-- Data for Name: ncq_workflow_variants; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ncq_workflow_variants (id_workflow, id_variant) FROM stdin;
\.


--
-- TOC entry 2834 (class 0 OID 16787)
-- Dependencies: 199
-- Data for Name: ncq_workflow_workflow_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ncq_workflow_workflow_categories (workflow_id_workflow, workflow_categories_id_category) FROM stdin;
1	1
1	2
1	3
\.


--
-- TOC entry 2842 (class 0 OID 0)
-- Dependencies: 200
-- Name: sequence_generator; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sequence_generator', 4001, true);


--
-- TOC entry 2704 (class 2606 OID 16783)
-- Name: ncq_workflow_category ncq_workflow_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ncq_workflow_category
    ADD CONSTRAINT ncq_workflow_category_pkey PRIMARY KEY (id_category);


--
-- TOC entry 2702 (class 2606 OID 16774)
-- Name: ncq_workflow ncq_workflow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ncq_workflow
    ADD CONSTRAINT ncq_workflow_pkey PRIMARY KEY (id_workflow);


--
-- TOC entry 2705 (class 2606 OID 16792)
-- Name: ncq_workflow_category fk8ajm5v42ir46t4i43sdjis04e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ncq_workflow_category
    ADD CONSTRAINT fk8ajm5v42ir46t4i43sdjis04e FOREIGN KEY (parent_id) REFERENCES public.ncq_workflow_category(id_category);


--
-- TOC entry 2709 (class 2606 OID 16812)
-- Name: ncq_workflow_workflow_categories fkbuuxqwnmde5iul6eiw38oj516; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ncq_workflow_workflow_categories
    ADD CONSTRAINT fkbuuxqwnmde5iul6eiw38oj516 FOREIGN KEY (workflow_id_workflow) REFERENCES public.ncq_workflow(id_workflow);


--
-- TOC entry 2708 (class 2606 OID 16807)
-- Name: ncq_workflow_workflow_categories fkct18vh4vfr2hmkcjioagsxtx0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ncq_workflow_workflow_categories
    ADD CONSTRAINT fkct18vh4vfr2hmkcjioagsxtx0 FOREIGN KEY (workflow_categories_id_category) REFERENCES public.ncq_workflow_category(id_category);


--
-- TOC entry 2707 (class 2606 OID 16802)
-- Name: ncq_workflow_variants fkgo6dkpt1em1t67d3rs42ml6k0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ncq_workflow_variants
    ADD CONSTRAINT fkgo6dkpt1em1t67d3rs42ml6k0 FOREIGN KEY (id_workflow) REFERENCES public.ncq_workflow(id_workflow);


--
-- TOC entry 2706 (class 2606 OID 16797)
-- Name: ncq_workflow_variants fkgs8421im1wh0f5wlixu4xufbe; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ncq_workflow_variants
    ADD CONSTRAINT fkgs8421im1wh0f5wlixu4xufbe FOREIGN KEY (id_variant) REFERENCES public.ncq_workflow(id_workflow);


-- Completed on 2020-04-19 21:16:59

--
-- PostgreSQL database dump complete
--

